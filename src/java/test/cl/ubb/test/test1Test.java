package cl.ubb.test;

import static org.junit.Assert.*;

import java.awt.List;
import java.util.ArrayList;

import org.junit.Test;

public class test1Test {

	@Test
	public void testPilaEsVacia() {
		ArrayList Lista = new ArrayList();
		
		Pila stack = new Pila();
		boolean resultado;
		resultado = stack.EstaVacia(Lista);
		assertEquals(true,resultado);
	}
	
	@Test
	public void testPilaEj2() {
		ArrayList Lista = new ArrayList();
		Pila stack = new Pila();
		boolean resultado;
		stack.push(Lista,1);
		resultado = stack.EstaVacia(Lista);
		assertEquals(false,resultado);
	}
	
	@Test
	public void testPilaEj3() {
		ArrayList Lista = new ArrayList();
		Pila stack = new Pila();
		boolean resultado;
		stack.push(Lista,1);
		stack.push(Lista,2);
		resultado = stack.EstaVacia(Lista);
		assertEquals(false,resultado);
	}
	
	@Test
	public void testPilaEj4() {
		ArrayList Lista = new ArrayList();
		Pila stack = new Pila();
		int resultado;
		stack.push(Lista,1);
		stack.push(Lista,2);
		resultado =stack.tama�o(Lista);
		assertEquals(2,resultado);
	}
	@Test
	public void testPilaEj5() {
		ArrayList Lista = new ArrayList();
		Pila stack = new Pila();
		int resultado;
		stack.push(Lista,1);
		resultado =stack.pop(Lista);
		assertEquals(1,resultado);
	}
	
	@Test
	public void testPilaEj6() {
		ArrayList Lista = new ArrayList();
		Pila stack = new Pila();
		int resultado;
		stack.push(Lista,1);
		stack.push(Lista,2);
		resultado =stack.pop(Lista);
		assertEquals(2,resultado);
	}
	@Test
	public void testPilaEj7() {
		ArrayList Lista = new ArrayList();
		Pila stack = new Pila();
		int resultado;
		stack.push(Lista,3);
		stack.push(Lista,4);
		resultado =stack.pop(Lista);
		assertEquals(4,resultado);
		resultado =stack.pop(Lista);
		assertEquals(3,resultado);
	}
	@Test
	public void testPilaEj8() {
		ArrayList Lista = new ArrayList();
		Pila stack = new Pila();
		int resultado;
		stack.push(Lista,1);
		resultado=stack.top(Lista);
		assertEquals(1,resultado);
	}
	@Test
	public void testPilaEj9() {
		ArrayList Lista = new ArrayList();
		Pila stack = new Pila();
		int resultado;
		stack.push(Lista,1);
		stack.push(Lista,2);
		resultado=stack.top(Lista);
		assertEquals(2,resultado);
	}
	
	
	

}
